// @flow

import Color from "../DataObject/Color";
import Vector3 from "../Math/Vector3";
import {clamp} from "../../Helper/Math/Clamp";

const TAU = 6.283185307179586;

export default class ColorPalette {
  #bias: Vector3;
  #scale: Vector3;
  #oscillations: Vector3;
  #phase: Vector3;

  #colors: Array<Color>;

  get bias() {
    return this.#bias;
  }

  get scale() {
    return this.#scale;
  }

  get oscillations() {
    return this.#oscillations;
  }

  get phase() {
    return this.#phase;
  }

  get colors() {
    return this.#colors;
  }

  constructor(bias: Vector3, scale: Vector3, oscillations: Vector3, phase: Vector3) {
    this.#bias         = bias;
    this.#scale        = scale;
    this.#oscillations = oscillations;
    this.#phase        = phase;

    this.#colors = [];
  }

  addColor(color: Color): void {
    this.#colors.push(color);
  }

  static generate(count: number, bias: Vector3, scale: Vector3, oscillations: Vector3, phase: Vector3, clampSaturation: boolean = false): ColorPalette {
    const palette = new ColorPalette(bias, scale, oscillations, phase);

    for (let i = 0; i < count; i++) {
      // $time never reaches 1, and that's ok because it prevents the same color at 0 and 1
      const time = i / count;

      // time = i / (count - 1); // Causes same color at 0 and 1

      const rawColor = ColorPalette.calculateChannels(time, bias, scale, oscillations, phase);

      let red   = parseInt(rawColor.x * 255);
      let green = parseInt(rawColor.y * 255);
      let blue  = parseInt(rawColor.z * 255);

      if (clampSaturation) {
        red   = clamp(red, 0, 255);
        green = clamp(green, 0, 255);
        blue  = clamp(blue, 0, 255);
      }

      // try {
        palette.addColor(new Color(red, green, blue));
      // } catch (RangeException exception) {
      //   throw new ValueException(
      //     'Sum of bias and scale is > 1.0, which oversaturates color channel',
      //     previous: $exception,
      // );
      // }
    }

    return palette;
  }

  static calculateChannels(time: number, bias: Vector3, scale: Vector3, oscillations: Vector3, phase: Vector3): Vector3 {
    return new Vector3(
      ColorPalette.calculateChannel(time, bias.x, scale.x, oscillations.x, phase.x),
      ColorPalette.calculateChannel(time, bias.y, scale.y, oscillations.y, phase.y),
      ColorPalette.calculateChannel(time, bias.z, scale.z, oscillations.z, phase.z),
    );
  }

  static calculateChannel(time: number, bias: number, scale: number, oscillations: number, phase): number {
    return bias + scale * Math.cos(TAU * (oscillations * time + phase));
  }

  toJSON() {
    return {
      bias: this.#bias,
      scale: this.#scale,
      oscillations: this.#oscillations,
      phase: this.#phase,
      colors: this.#colors,
    };
  }
}
