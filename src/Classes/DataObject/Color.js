// @flow

export default class Color {
  #red:   number;
  #green: number;
  #blue:  number;

  get red() {
    return this.#red;
  }

  get green() {
    return this.#green;
  }

  get blue() {
    return this.#blue;
  }

  constructor(red: number, green: number, b: number) {
    this.#red = red;
    this.#green = green;
    this.#blue = b;
  }

  toString(decimalPlaces: number = 2): string {
    const red   = `${this.#red.toFixed(decimalPlaces)}`;
    const green = `${this.#green.toFixed(decimalPlaces)}`;
    const blue  = `${this.#blue.toFixed(decimalPlaces)}`;

    return `(${red}, ${green}, ${blue})`;
  }

  toHex(): string {
    const paddedRed   = this.#red.toString(16).padStart(2, '0');
    const paddedGreen = this.#green.toString(16).padStart(2, '0');
    const paddedBlue  = this.#blue.toString(16).padStart(2, '0');

    return `#${paddedRed}${paddedGreen}${paddedBlue}`;
  }

  toJSON() {
    return {
      red:   this.#red,
      green: this.#green,
      blue:  this.#blue,
    };
  }
}
