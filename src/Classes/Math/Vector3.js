// @flow

export default class Vector3 {
  #x: number;
  #y: number;
  #z: number;

  get x() {
    return this.#x;
  }

  get y() {
    return this.#y;
  }

  get z() {
    return this.#z;
  }

  constructor(x: number, y: number, z: number) {
    this.#x = x;
    this.#y = y;
    this.#z = z;
  }

  toString(decimalPlaces: number = 2): string {
    const x = `${this.#x.toFixed(decimalPlaces)}`;
    const y = `${this.#y.toFixed(decimalPlaces)}`;
    const z = `${this.#z.toFixed(decimalPlaces)}`;

    return `(${x}, ${y}, ${z})`;
  }

  toJSON() {
    return {
      x: this.#x,
      y: this.#y,
      z: this.#z,
    };
  }

  static add(a: Vector3, b: Vector3): Vector3 {
    return new Vector3(
      a.x + b.x,
      a.y + b.y,
      a.z + b.z,
    );
  }

  static subtract(a: Vector3, b: Vector3): Vector3 {
    return new Vector3(
      a.x - b.x,
      a.y - b.y,
      a.z - b.z,
    );
  }

  multiply(factor: number): Vector3 {
    return new Vector3(
      this.#x * factor,
      this.#y * factor,
      this.#z * factor,
    );
  }
}
