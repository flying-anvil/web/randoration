// @flow

import type {ColorHex} from "./Color";

export type ExportMetaType = {
  name: string,
}

export type ExportValueType = {
  name: string,
  value: string,
  color: ColorHex,
}

export type ExportType = {
  meta: ExportMetaType,
  values: {
    [string]: ExportValueType,
  },
}
