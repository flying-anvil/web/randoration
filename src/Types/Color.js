// @flow

export type Color = {
  red: number,
  green: number,
  blue: number,
  alpha: ?number,
}

export type ColorHSV = {
  hue: number,
  saturation: number,
  value: number,
  alpha: ?number,
}

export type ColorHex = string
