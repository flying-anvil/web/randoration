// @flow

import Vector3 from "../../Classes/Math/Vector3";

export type PresetType = {
  name: string,
  bias: Vector3,
  scale: Vector3,
  oscillations: Vector3,
  phase: Vector3,
}

const presetDefault: PresetType = {
  name: 'Default',
  bias:         new Vector3(0.50, 0.50, 0.50),
  scale:        new Vector3(0.50, 0.50, 0.50),
  oscillations: new Vector3(1.00, 1.00, 1.00),
  phase:        new Vector3(0.00, 0.33, 0.66),
}

const preset1: PresetType = {
  name: 'Brown Blue',
  bias:         new Vector3(0.50, 0.50, 0.50),
  scale:        new Vector3(0.50, 0.50, 0.50),
  oscillations: new Vector3(1.00, 1.00, 1.00),
  phase:        new Vector3(0.00, 0.10, 0.20),
}

const preset2: PresetType = {
  name: 'Metallic Rust',
  bias:         new Vector3(0.50, 0.50, 0.50),
  scale:        new Vector3(0.50, 0.50, 0.50),
  oscillations: new Vector3(1.00, 1.00, 1.00),
  phase:        new Vector3(0.30, 0.20, 0.20),
}

const preset3: PresetType = {
  name: 'Wet Honey',
  bias:         new Vector3(0.50, 0.50, 0.50),
  scale:        new Vector3(0.50, 0.50, 0.50),
  oscillations: new Vector3(1.00, 1.00, 1.00),
  phase:        new Vector3(0.80, 0.90, 0.30),
}

const preset4: PresetType = {
  name: 'Honey Sweetheart',
  bias:         new Vector3(0.50, 0.50, 0.50),
  scale:        new Vector3(0.50, 0.50, 0.50),
  oscillations: new Vector3(1.00, 0.70, 0.40),
  phase:        new Vector3(0.00, 0.15, 0.20),
}

const preset5: PresetType = {
  name: 'Neon Something',
  bias:         new Vector3(0.50, 0.50, 0.50),
  scale:        new Vector3(0.50, 0.50, 0.50),
  oscillations: new Vector3(2.00, 1.00, 0.00),
  phase:        new Vector3(0.50, 0.20, 0.25),
}

const preset6: PresetType = {
  name: 'Cool Peach',
  bias:         new Vector3(0.80, 0.50, 0.40),
  scale:        new Vector3(0.20, 0.40, 0.20),
  oscillations: new Vector3(2.00, 1.00, 1.00),
  phase:        new Vector3(0.00, 0.25, 0.25),
}

// My

const presetLeaves: PresetType = {
  name: 'Leaves',
  bias:         new Vector3(0.30, 0.00, 0.22),
  scale:        new Vector3(0.25, 0.75, 0.22),
  oscillations: new Vector3(1.30, 0.25, 0.66),
  phase:        new Vector3(0.05, 0.00, 0.00),
}

const presetAlien: PresetType = {
  name: 'Alien',
  bias:         new Vector3(0.50, 0.50, 0.90),
  scale:        new Vector3(0.50, 0.50, 0.65),
  oscillations: new Vector3(1.00, 1.00, 0.40),
  phase:        new Vector3(0.80, 0.90, 0.10),
}

const presetCityInTheNight: PresetType = {
  name: 'City in the Night',
  bias:         new Vector3(0.52, 0.40, 0.45),
  scale:        new Vector3(0.30, 0.42, 0.19),
  oscillations: new Vector3(1.10, 0.60, 0.75),
  phase:        new Vector3(0.22, 0.60, 0.75),
}

const all = [
  presetDefault,
  preset1,
  preset2,
  preset3,
  preset4,
  preset5,
  preset6,
  presetLeaves,
  presetAlien,
  presetCityInTheNight,
]

export default all;
