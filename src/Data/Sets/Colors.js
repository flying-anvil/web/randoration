// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";
import {SetWeight} from "./Constants";

/* Template
{
  name: 'Template',
  weight: 3,
  color: '#666666',
}
 */

const RawColors: SetType = {
  meta: {
    name: 'Color',
  },
  values: [
    {
      name: 'Cyan',
      weight: SetWeight.normal,
      color: '#38D0CC',
    },
    {
      name: 'Brown',
      weight: SetWeight.normal,
      color: '#924745',
    },
    {
      name: 'Green',
      weight: SetWeight.normal,
      color: '#2E7A2B',
    },
    {
      name: 'Orange',
      weight: SetWeight.normal,
      color: '#FFA500',
    },
    {
      name: 'Gold',
      weight: SetWeight.normal,
      color: '#FFD700',
    },
    {
      name: 'Grey',
      weight: SetWeight.normal,
      color: '#808080',
    },
    {
      name: 'Purple',
      weight: SetWeight.normal,
      color: '#7A2B77',
    },
    {
      name: 'Magenta',
      weight: SetWeight.normal,
      color: '#CC30C7',
    },
    {
      name: 'Red',
      weight: SetWeight.normal,
      color: '#C92D2B',
    },
    {
      name: 'White',
      weight: SetWeight.normal,
      color: '#F8F8FF',
    },
    {
      name: 'Teal',
      weight: SetWeight.normal,
      color: '#307B79',
    },
  ],
}

const addVariants = (set: SetType): SetType => {
  const variants = [
    'Bright',
    'Dark',
    'Pale',
    'Shining',
  ];

  const newValues = [...set.values];

  for (const setValue of set.values) {
    for (const variant of variants) {
      newValues.push({
        name: `${variant} ${setValue.name}`,
        weight: setValue.weight * .75,
        color: setValue.color, // TODO: modify based on variant
      });
    }
  }

  return {
    meta: set.meta,
    values: newValues,
  }
}

const Elements = addVariants(fillWithDefaultValues(RawColors));

export {RawColors};
export default Elements;
