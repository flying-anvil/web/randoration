// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";

/* Template
{
  name: 'Template',
  weight: 3,
  color: '#666666',
}
 */

const RawWeathers: SetType = {
  meta: {
    name: 'Weather',
  },
  values: [
    // Airy
    {
      name: 'Clear',
      weight: 4,
      color: '#b4ccef',
    },
    {
      name: 'Cloudy',
      weight: 3,
      color: '#e4e8e8',
    },
    {
      name: 'Windy',
      weight: 4,
      color: '#829ab3',
    },
    {
      name: 'Hurricane',
      weight: 1,
      color: '#8d8d8d',
    },
    {
      name: 'Storm',
      weight: 3,
      color: '#738299',
    },
    {
      name: 'Thunder Storm',
      weight: 1,
      color: '#544A4F',
    },
    {
      name: 'Sand Storm',
      weight: 1,
      color: '#a59d71',
    },
    {
      name: 'Fire Storm',
      weight: 1,
      color: '#8f3939',
    },

    // Wet
    {
      name: 'Rain',
      weight: 4,
      color: '#a0c0c8',
    },
    {
      name: 'Drizzle',
      weight: 2,
      color: '#789aba',
    },
    {
      name: 'Fog',
      weight: 3,
      color: '#AAAAAAAA',
    },
    {
      name: 'Snow',
      weight: 3,
      color: '#dbf3f3',
    },
    {
      name: 'Hail',
      weight: 2,
      color: '#bcebeb',
    },
    {
      name: 'Blizzard',
      weight: 1,
      color: '#9cf5f5',
    },

    // Special
    {
      name: 'Blood Rain',
      weight: 1,
      color: '#86090b',
    },
  ],
}

const Weathers = fillWithDefaultValues(RawWeathers);

export {RawWeathers};
export default Weathers;
