// @flow

import type {Color, ColorHex} from "../../../Types/Color";

export type SetValueType = {
  name: string,
  weight?: number,
  color?: ColorHex | Color,
}

export type SetMetaType = {
  name: string,
}

export type SetType = {
  meta: SetMetaType,
  values: Array<SetValueType>,
}
