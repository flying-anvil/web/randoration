// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";

/* Template
{
  name: 'Template',
  weight: 3,
  color: '#666666',
}
 */

const RawElements: SetType = {
  meta: {
    name: 'Element',
  },
  values: [
    {
      name: 'Hot',
      weight: 4,
      color: '#955a3a',
    },
    {
      name: 'Burning',
      weight: 1,
      color: '#bd5a48',
    },
    {
      name: 'Flooded',
      weight: 4,
      color: '#3773a0',
    },
  ],
}

const Elements = fillWithDefaultValues(RawElements);

export {RawElements};
export default Elements;
