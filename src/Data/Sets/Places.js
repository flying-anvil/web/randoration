// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";
import {SetWeight} from "./Constants";

/* Template
{
  name: 'Template',
  weight: 3,
  color: '#666666',
}
 */

const RawPlaces: SetType = {
  meta: {
    name: 'Place',
  },
  values: [
    {
      name: 'Junkyard',
      weight: SetWeight.normal,
      color: '#78451B',
    },
    {
      name: 'Graveyard',
      weight: SetWeight.normal,
      color: '#352c3b',
    },
    {
      name: 'Shipyard',
      weight: SetWeight.normal,
      color: '#304682',
    },
    {
      name: 'Castle',
      weight: SetWeight.normal,
      color: '#5d5e57',
    },
    {
      name: 'Factory',
      weight: SetWeight.normal,
      color: '#71767A',
    },
    {
      name: 'Pyramid',
      weight: SetWeight.normal,
      color: '#E6D1AB',
    },
    {
      name: 'Mine',
      weight: SetWeight.normal,
      color: '#1E1E1E',
    },
    {
      name: 'Amusement Park',
      weight: SetWeight.normal,
      color: '#8d789b',
    },
    {
      name: 'Haunted House',
      weight: SetWeight.normal,
      color: '#082838',
    },
    {
      name: 'Air Ship',
      weight: SetWeight.normal,
      color: '#88631a',
    },
    {
      name: 'Sewer',
      weight: SetWeight.normal,
      color: '#373D1A',
    },
    {
      name: 'Temple',
      weight: SetWeight.normal,
      color: '#5E6056',
    },
    {
      name: 'Ruins',
      weight: SetWeight.normal,
      color: '#32332E',
    },
    {
      name: 'Laboratory',
      weight: SetWeight.normal,
      color: '#297B6B',
    },
  ],
}

const Places = fillWithDefaultValues(RawPlaces);

export {RawPlaces};
export default Places;
