// @flow

export const SetWeight = {
  veryLow: 100,
  low: 200,
  subNormal: 400,
  normal: 500,
  supNormal: 600,
  high: 800,
  veryHigh: 900,
}
