// @flow

import {fillWithDefaultValues} from "../../../Helper/SetFormatter";
import type {SetType} from "../Types/SetType";
import {SetWeight} from "../Constants";

/* Template
{
  name: 'Template',
  weight: 3,
  color: '#666666',
}
 */

const RawTylesetTypes: SetType = {
  meta: {
    name: 'Typeset Type',
  },
  values: [
    {
      name: 'Regular',
      weight: SetWeight.normal,
      color: '#AAAAAA',
    },
    {
      name: 'Pipe',
      weight: SetWeight.veryLow,
      color: '#999999',
    },
    {
      name: 'Decoration',
      weight: SetWeight.veryLow,
      color: '#bbbbbb',
    },
    {
      name: 'Block',
      weight: SetWeight.low,
      color: '#747174',
    },
    {
      name: 'Overworld',
      weight: SetWeight.veryLow,
      color: '#DECEAD',
    },
  ],
}

const Elements = fillWithDefaultValues(RawTylesetTypes);

export {RawTylesetTypes};
export default Elements;
