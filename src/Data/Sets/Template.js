// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";
import {SetWeight} from "./Constants";

/* Template
{
  name: 'Template',
  weight: SetWeight.normal,
  color: '#666666',
}
 */

const RawTemplate: SetType = {
  meta: {
    name: 'Template',
  },
  values: [
    {
      name: 'Template',
      weight: SetWeight.normal,
      color: '#666666',
    },
  ],
}

const Elements = fillWithDefaultValues(RawTemplate);

export {RawTemplate};
export default Elements;
