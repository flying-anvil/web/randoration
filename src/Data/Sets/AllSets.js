// @flow

import Biomes from "./Biomes";
import Themes from "./Themes";
import Elements from "./Elements";
import type {SetType} from "./Types/SetType";
import Weathers from "./Weather";
import Places from "./Places";
import TilesetType from "./Tileset/TilesetType";
import Colors from "./Colors";
import HtmlColors from "./HtmlColors";
import Materials from "./Materials";

const setNameMapping = {
  [Biomes.meta.name.toLowerCase()]: Biomes,
  [Themes.meta.name.toLowerCase()]: Themes,
  [Elements.meta.name.toLowerCase()]: Elements,
  [Weathers.meta.name.toLowerCase()]: Weathers,
  [Places.meta.name.toLowerCase()]: Places,
  [Materials.meta.name.toLowerCase()]: Materials,

  [Colors.meta.name.toLowerCase()]: Colors,
  [HtmlColors.meta.name.toLowerCase()]: HtmlColors,

  // Tileset specific
  [TilesetType.meta.name.toLowerCase()]: TilesetType,
}

export function setNameToSet(setName: string): SetType {
  return setNameMapping[setName.toLowerCase()] || null;
}

export {
  setNameMapping as AllSets,
}
