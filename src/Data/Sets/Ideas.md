# Tileset

| Name                | Color | Category      |
|---------------------|-------|---------------|
| Snow                |       | Material      |
| Ice                 |       | Material      |
| Mountain            |       | Theme/Purpose |
| Castle              |       | Place         |
| Ghost House         |       | Place         |
| Air Ship            |       | Place         |
| Mine                |       | Place         |
| Desert              |       | Theme/Purpose |
| Beach               |       | Theme/Purpose |
| Jungle              |       | Theme/Purpose |
| Swamp               |       | Theme/Purpose |
| Space               |       | Theme/Purpose |
| Natural             |       | Theme/Purpose |
| Sewer               |       | Place         |
| Temple              |       | Place         |
| Ruins               |       | Place         |
| Forest              |       | Theme/Purpose |
| Cave                |       | Theme/Purpose |
| Factory             |       | Place         |
| Spooky/Haunted      |       | Style         |
| Laboratory          |       | Place         |
| Sky                 |       | Theme/Purpose |
| Grass               |       | Material      |
| Dirt                |       | Material      |
| Brick               |       | Material      |
| Wood                |       | Material      |
| Rock                |       | Material      |
| Crystal             |       | Material      |
| Flesh               |       | Material      |
| Natual              |       | Material      |
| Alien               |       | Material      |
| Candy               |       | Material      |
| Steampunk           |       | Style         |
| Fire/Fiery          |       | Style         |
| Candy               |       | Style         |
| Industrial          |       | Style         |
| Digital             |       | Style         |
| Alien               |       | Style         |
| Overgrown           |       | Style         |
| Happy/Bright        |       | Style         |
| Dark/Depressing     |       | Style         |
| Natual              |       | Style         |
| Artificial/Man-Made |       | Style         |
| Sand                |       | Material      |
| Sandstone           |       | Material      |
| Metal               |       | Material      |
| Gemstone            |       | Material      |
|                     |       |               |
|                     |       |               |
|                     |       |               |

## Stuff

| Stuff      |
|------------|
| Regular    |
| Pipe       |
| Decoration |
| Block      |
| Overworld  |
|            |

# Colors

Primary, Secondary

| Color   |
|---------|
| Cyan    |
| Brown   |
| Green   |
| Orange  |
| Gold    |
| Grey    |
| Purple  |
| Magenta |
| Red     |
| White   |
| Teal    |
|         |
|         |
|         |

# Color Modifier

| Modifier |
|----------|
| Bright   |
| Dark     |
| Regular  |
| Pale     |
| Shining  |
|          |
|          |
