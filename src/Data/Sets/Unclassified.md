| Name                        | Color             | Idea                                            |
|-----------------------------|-------------------|-------------------------------------------------|
| Factory                     | Metallic          | Structure                                       |
| Pyramid                     | Sandstone/Yellow  | Structure                                       |
| Mine                        | Stone/Coal        | Structure / Natural Speciality                  |
| Amusement Park              | #a48eb4           | Structure                                       |
| Castle                      |                   | Structure                                       |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
|                             |                   |                                                 |
