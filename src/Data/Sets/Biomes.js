// @flow

import type {SetType} from "./Types/SetType";
import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import {SetWeight} from "./Constants";

/* Template
{
  name: 'Template',
  weight: SetWeight.normal,
  color: '#666666',
}
 */

// TODO: Find better name instead of "Biomes"
const RawBiomes: SetType = {
  meta: {
    name: 'Biome',
  },
  values: [
    // Aquatic
    {
      name: 'Beach',
      weight: SetWeight.normal,
      color: '#D8D2CA',
    },
    {
      name: 'Mangrove',
      weight: SetWeight.normal,
      color: '#3A6476',
    },
    {
      name: 'Bog',
      weight: SetWeight.normal,
      color: '#193940',
    },
    {
      name: 'Swamp',
      weight: SetWeight.normal,
      color: '#333C22',
    },
    {
      name: 'Lake',
      weight: SetWeight.normal,
      color: '#0156aa',
    },

    // Grassland
    {
      name: 'Field',
      weight: SetWeight.subNormal,
      color: '#65B43E',
      // color: '#37e62e'
    },
    {
      name: 'Plains',
      weight: SetWeight.subNormal,
      color: '#49872d',
      // color: '#37e62e'
    },

    // Forest
    {
      name: 'Forest',
      weight: SetWeight.normal,
      color: '#37673E',
      // color: '#0f6120'
    },
    {
      name: 'Jungle',
      weight: SetWeight.normal,
      color: '#155B00',
      // color: '#0f6120'
    },

    // Desert
    {
      name: 'Desert',
      weight: SetWeight.supNormal,
      color: '#B5B23A',
      // color: '#d0ce0f'
    },
    {
      name: 'Savannah',
      weight: SetWeight.supNormal,
      color: '#C2B08A',
    },

    // Tundra
    {
      name: 'Tundra',
      weight: SetWeight.normal,
      color: '#b7efec',
    },

    // Airborne
    {
      name: 'Sky',
      weight: SetWeight.normal,
      color: '#88CCFF',
    },
    {
      name: 'Clouds',
      weight: SetWeight.normal,
      color: '#D6E6F7',
    },

    // Mountain
    {
      name: 'Badlands',
      weight: SetWeight.low,
      color: '#C67566',
    },
    {
      name: 'Canyon',
      weight: SetWeight.subNormal,
      color: '#E6885F',
    },

    // Rocky
    {
      name: 'Mountain',
      weight: SetWeight.normal,
      color: "#818B99",
    },

    // Misc
    {
      name: 'Cave',
      weight: SetWeight.normal,
      color: '#424242',
    },
    {
      name: 'Space',
      weight: SetWeight.normal,
      color: '#153951',
    },
  ],
};

const Biomes = fillWithDefaultValues(RawBiomes);

export {RawBiomes};
export default Biomes;

