// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";
import {SetWeight} from "./Constants";

/* Template
{
  name: 'Template',
  weight: SetWeight.normal,
  color: '#666666',
}
 */

const RawMaterial: SetType = {
  meta: {
    name: 'Material',
  },
  values: [
    {
      name: 'Snow',
      weight: SetWeight.normal,
      color: '#E9F6F6FC',
    },
    {
      name: 'Ice',
      weight: SetWeight.normal,
      color: '#b7efec',
    },
    {
      name: 'Grass',
      weight: SetWeight.normal,
      color: '#348C31',
    },
    {
      name: 'Dirt',
      weight: SetWeight.normal,
      color: '#9B7653',
    },
    {
      name: 'Brick',
      weight: SetWeight.normal,
      color: '#BC4A3C',
    },
    {
      name: 'Wood',
      weight: SetWeight.normal,
      color: '#BA8C63',
    },
    {
      name: 'Rock',
      weight: SetWeight.normal,
      color: '#817669',
    },
    {
      name: 'Crystal',
      weight: SetWeight.normal,
      color: '#5EB0CF',
    },
    {
      name: 'Flesh',
      weight: SetWeight.normal,
      color: '#EA8F83',
    },
    {
      name: 'Natual',
      weight: SetWeight.low,
      color: '#369f33',
    },
    {
      name: 'Alien',
      weight: SetWeight.low,
      color: '#4F8A39',
    },
    {
      name: 'Candy',
      weight: SetWeight.normal,
      color: '#E0A2B4',
    },
    {
      name: 'Sand',
      weight: SetWeight.normal,
      color: '#D7A83E',
    },
    {
      name: 'Sandstone',
      weight: SetWeight.normal,
      color: '#BFA674',
    },
    {
      name: 'Metal',
      weight: SetWeight.normal,
      color: '#5C5D5B',
    },
    {
      name: 'Gemstone',
      weight: SetWeight.normal,
      color: '#289D8C',
    },
  ],
}

const Elements = fillWithDefaultValues(RawMaterial);

export {RawMaterial};
export default Elements;
