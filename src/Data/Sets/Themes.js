// @flow

import {fillWithDefaultValues} from "../../Helper/SetFormatter";
import type {SetType} from "./Types/SetType";
import {SetWeight} from "./Constants";

/* Template
{
  name: 'Template',
  weight: 3,
  color: '#666666',
}
 */

const RawThemes: SetType = {
  meta: {
    name: 'Theme',
  },
  values: [
    {
      name: 'Ruined',
      weight: SetWeight.normal,
      color: '#131d2d',
    },
    {
      name: 'Abandoned',
      weight: SetWeight.normal,
      color: '#38403c',
    },
    {
      name: 'Buried',
      weight: SetWeight.normal,
      color: '#443828',
    },
    {
      name: 'Ancient',
      weight: SetWeight.normal,
      color: '#B48D60',
    },
    {
      name: 'Artificial',
      weight: SetWeight.normal,
      color: '#26878c',
    },
    {
      name: 'Futuristic',
      weight: SetWeight.normal,
      color: '#8be0f4',
    },
    {
      name: 'Overgrown',
      weight: SetWeight.normal,
      color: '#31792d',
    },
    {
      name: 'Steampunk',
      weight: SetWeight.normal,
      color: '#DFB982',
    },
    {
      name: 'Vulcanic',
      weight: SetWeight.normal,
      color: '#B5332E',
    },
    {
      name: 'Industrial',
      weight: SetWeight.normal,
      color: '#CED2D7',
    },
    {
      name: 'Digital',
      weight: SetWeight.normal,
      color: '#4EBCC0',
    },
    {
      name: 'Happy/Bright',
      weight: SetWeight.normal,
      color: '#CCCCCC ',
    },
    {
      name: 'Depressing/Dark',
      weight: SetWeight.normal,
      color: '#444444 ',
    },
  ],
}

const Themes = fillWithDefaultValues(RawThemes);

export {RawThemes};
export default Themes;
