// @flow

import {useEffect, useRef} from "react";

export type Delta = {
  ms: number,
  s: number,
  m: number,
}

export function useAnimationFrame(enabled: boolean, animation: Function<Delta>) {
  const requestRef      = useRef();
  const previousTimeRef = useRef();

  useEffect(() => {
    const doAnimate = (time: number) => {
      if (!enabled) {
        previousTimeRef.current = undefined;
        return;
      }

      if (previousTimeRef.current !== undefined) {
        const rawDelta = time - previousTimeRef.current
        const delta: Delta = {
          ms: rawDelta,
          s: rawDelta * .001,
          m: (rawDelta * 0.001) / 60,
        }

        animation(delta);
      }

      previousTimeRef.current = time;
      requestRef.current = requestAnimationFrame(doAnimate);
    };

    // Start animation loop
    requestRef.current = requestAnimationFrame(doAnimate);

    return () => cancelAnimationFrame(requestRef.current);
  }, [enabled, animation]);
}
