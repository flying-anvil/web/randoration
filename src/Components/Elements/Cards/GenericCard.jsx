// @flow
import React, {useCallback, useLayoutEffect, useState} from 'react';
import {pickRandomFromSet} from "../../../Helper/SetPicker";
import type {SetMetaType, SetType, SetValueType} from "../../../Data/Sets/Types/SetType";
import RandorationCard from "./RandorationCard";

type Props = {
  value?: SetValueType,
  additionalContent?: null | string | Function,
  set: SetType,
  onChange?: Function<SetValueType, ?SetMetaType>,
  nameOverride?: string,
};

export default function GenericCard(props: Props) {
  const preSet: SetValueType = props.value;
  const {additionalContent, set, onChange, nameOverride} = props;

  const notifyChange = useCallback((newValue) => {
    if (onChange) {
      onChange(newValue, set.meta, nameOverride || null);
    }
  }, [onChange, set.meta, nameOverride]);

  // if (!preSet && !set) {
  //   throw new Error('Either set or value must be given');
  // }

  const [picked, setPicked] = useState(preSet);
  const [locked, setLocked] = useState(false);

  // Layout Effect prevents flickering
  useLayoutEffect(() => {
    if (!picked) {
      const newPicked = pickRandomFromSet(set)
      setPicked(newPicked);

      notifyChange(newPicked);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const generate = useCallback(() => {
    if (preSet || locked) {
      return;

    }
    const newPicked = pickRandomFromSet(set);

    setPicked(newPicked);
    notifyChange(newPicked);
  }, [preSet, set, notifyChange, locked]);

  const lock = useCallback(() => {
    setLocked((old: boolean) => !old);
  }, []);

  if (!picked) {
    return null;
  }

  return (
    <RandorationCard
      color={picked.color}
      title={nameOverride || set.meta.name}
      value={picked.name}
      additionalContent={additionalContent}
      generate={!preSet && generate}
      lock={!preSet && lock}
      locked={locked}
    />
  );
};
