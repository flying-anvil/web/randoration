// @flow
import React, {useCallback} from 'react';
import {Button, Card, Col, Row} from "react-bootstrap";
import type {ColorHex} from "../../../Types/Color";
import {BiLock, BiLockOpen, BiRefresh} from "react-icons/all";
import {brightenDarkHexColor} from "../../../Helper/ColorConversion";

type Props = {
  color: ColorHex,
  title: string,
  value: string,
  additionalContent?: null | string | Function,
  generate?: ?Function,
  lock?: ?Function,
  locked?: ?boolean,
};

export default function RandorationCard(props: Props) {
  const {color, title, value, additionalContent = null, generate, lock, locked = false} = props;

  const renderAdditionalContent = useCallback(() => {
    if (typeof additionalContent === 'function') {
      return additionalContent(value, color);
    }
  }, [additionalContent, color,  value]);

  return (
    <Card style={{background: color, color: brightenDarkHexColor(color)}} className={'text-invert'}>
      <Card.Body>
        <Row>
          <Col>
            <Card.Title>{title}</Card.Title>
          </Col>
          {/*<Col>*/}
          {/*  {generate &&*/}
          {/*  <Col className={'text-right'}>*/}
          {/*    <Button variant={'outline-secondary'} size={'sm'} onClick={generate}><BiRefresh/></Button>*/}
          {/*  </Col>*/}
          {/*  }*/}
          {/*</Col>*/}
        </Row>
        <Row>
          <Col>
            <Card.Text as={'div'}>
              <div>{value}</div>
              {additionalContent &&
                <div>{renderAdditionalContent()}</div>
              }
            </Card.Text>
          </Col>
          {(generate || lock) &&
            <Col className={'text-right align-text-bottom'}>
              {(generate && !locked) && (
                <Button className={'ml-2'} variant={'outline-secondary'} size={'sm'} onClick={generate}>
                  <BiRefresh/>
                </Button>
              )}
              {lock && (
                <Button className={'ml-2'} variant={'outline-secondary'} size={'sm'} onClick={lock}>
                  {locked ? <BiLock/> : <BiLockOpen/>}
                </Button>
              )}
            </Col>
          }
        </Row>
      </Card.Body>
    </Card>
  );
};
