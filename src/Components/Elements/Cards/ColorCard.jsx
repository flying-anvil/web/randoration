// @flow
import React from 'react';
import GenericCard from "./GenericCard";
import type {SetMetaType, SetType, SetValueType} from "../../../Data/Sets/Types/SetType";
import {Col, Row} from "react-bootstrap";
import {color1toColor255, hexToColor, RGBtoHSV, RGBtoLuminancePerceived} from "../../../Helper/ColorConversion";

type Props = {
  value?: SetValueType,
  set: SetType,
  onChange?: Function<SetValueType, ?SetMetaType>,
  nameOverride?: string,
};

export default function ColorCard(props: Props) {
  const {value, set, onChange, nameOverride} = props;

  return (
    <GenericCard
      value={value}
      set={set}
      onChange={onChange}
      nameOverride={nameOverride}
      additionalContent={(value, color) => {
        const {red, green, blue} = hexToColor(color);
        const colorHsv = RGBtoHSV(red, green, blue);

        const hue = colorHsv.hue.toFixed(0);
        const saturation = (colorHsv.saturation * 100).toFixed(0);
        const hsvValue = (colorHsv.value * 100).toFixed(0);

        const luminance = (RGBtoLuminancePerceived(red / 255, green / 255, blue / 255) * 100).toFixed(0);

        return (
          <Col>
            <Row>Hex: {color}</Row>
            <Row>RGB: ({red}, {green}, {blue})</Row>
            <Row>HSV: ({hue}°, {saturation}%, {hsvValue}%)</Row>
            <Row>Luminance: {luminance}%</Row>
          </Col>
        )
      }}/>
  );
}
