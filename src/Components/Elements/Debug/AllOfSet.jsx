// @flow
import React from 'react';
import {Card, Col, Row} from "react-bootstrap";
import GenericCard from "../Cards/GenericCard";
import Biomes from "../../../Data/Sets/Biomes";
import SetWeightDistribution from "./SetWeightDistribution";
import type {SetType} from "../../../Data/Sets/Types/SetType";
import Themes from "../../../Data/Sets/Themes";
import Elements from "../../../Data/Sets/Elements";
import HtmlColors from "../../../Data/Sets/HtmlColors";
import ColorCard from "../Cards/ColorCard";
import Colors from "../../../Data/Sets/Colors";

type Props = {
  set: SetType,
};

const setNameToCardTag = (setName: string) => {
  switch (setName) {
    case Biomes.meta.name:
      return GenericCard;
    case Themes.meta.name:
      return GenericCard;
    case Elements.meta.name:
      return GenericCard;
    case Colors.meta.name:
      return ColorCard;
    case HtmlColors.meta.name:
      return ColorCard;
    default:
      return GenericCard;
  }
}

export default function AllOfSet(props: Props) {
  const {set} = props;
  const SetCard = setNameToCardTag(set.meta.name);

  return (
    <>
      <Row className={'ml-5 mr-5'}>
        <Col md={1}/>
        <Col md={10}>
          <Card>
            <Card.Header>
              <Card.Title>All {set.meta.name}s</Card.Title>
            </Card.Header>
            <Card.Body>
              <Row>
                {set.values.map((value) => (
                  <Col key={`${value.name}_${value.color}`} className={'mb-4'}>
                    <SetCard value={value} set={set}/>
                  </Col>))}
              </Row>
            </Card.Body>
          </Card>
        </Col>
        <Col md={1}/>
      </Row>

      <Row className={'mt-3'}>
        <Col md={12}>
          <SetWeightDistribution set={set}/>
        </Col>
      </Row>
    </>
  );
};
