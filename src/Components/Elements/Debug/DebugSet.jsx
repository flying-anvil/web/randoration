// @flow
import React from 'react';
import {useParams} from "react-router-dom";
import {setNameToSet} from "../../../Data/Sets/AllSets";
import AllOfSet from "./AllOfSet";

type Props = {};

export default function DebugSet(props: Props) {
  const {set: setName} = useParams();

  const set = setNameToSet(setName);

  if (!set) {
    return null;
  }

  return <AllOfSet set={set}/>
};
