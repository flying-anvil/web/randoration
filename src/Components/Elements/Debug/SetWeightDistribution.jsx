// @flow
import React from 'react';
import {Card} from "react-bootstrap";
import {Bar, BarChart, CartesianGrid, Cell, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import type {SetType, SetValueType} from "../../../Data/Sets/Types/SetType";

type Props = {
  set: SetType,
};

export default function SetWeightDistribution(props: Props) {
  const {set} = props;

  return (
    <Card>
      <Card.Header>
        <Card.Title>{set.meta.name} weight distribution</Card.Title>
      </Card.Header>
      <Card.Body>
        <div style={{height: 275}}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart data={set.values} barCategoryGap={'1'}>
              <Bar dataKey="weight">
                {
                  set.values.map((value: SetValueType) => (
                    <Cell
                      key={value.name}
                      fill={value.color}
                      stroke={value.color}
                      strokeOpacity={0.5}
                      strokeWidth={2}
                    />
                  ))
                }
              </Bar>

              <XAxis dataKey="name" padding={{left: 0, right: 0}}/>
              <YAxis/>
              <Tooltip/>
              <CartesianGrid strokeDasharray="2 3" strokeOpacity={0.5}/>
            </BarChart>
          </ResponsiveContainer>
        </div>
      </Card.Body>
    </Card>
  );
};
