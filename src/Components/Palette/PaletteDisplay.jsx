// @flow
import React from 'react';
import Color from "../../Classes/DataObject/Color";
import {Col, Row} from "react-bootstrap";

type Props = {
  colors: Array<Color>,
};

export default function PaletteDisplay(props: Props) {
  const {colors} = props;

  return (
    <Row>
      <Col>
        {colors.map((color: Color, index: number) => {
          const colorHex = color.toHex();

          return (
            <div key={`${index}.${colorHex}`} style={{backgroundColor: colorHex, float: 'left', width: 24, height: 24}}>
              &nbsp;
            </div>
          );
        })}
      </Col>
    </Row>
  );
};
