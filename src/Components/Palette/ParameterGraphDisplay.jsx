// @flow
import React from 'react';
import type {ParametersState} from "./parameterReducer";
import {LineChart, Line, ReferenceLine, ResponsiveContainer, YAxis} from "recharts";
import ColorPalette from "../../Classes/Palette/ColorPalette";
import {clamp01} from "../../Helper/Math/Clamp";
// import {curveCardinal} from "d3-shape";

type Props = {
  parameters: ParametersState,
  clampSaturation?: boolean,
  precision?: number,
};

export default function ParameterGraphDisplay(props: Props) {
  const {
    parameters,
    clampSaturation = true,
    precision = 25,
  } = props;

  const {bias, scale, oscillations, phase} = parameters;

  const points = [];

  for (let i = 0; i < precision; i++) {
    const time = i / precision;

    let rawRed   = ColorPalette.calculateChannel(time,  bias.x, scale.x, oscillations.x, phase.x);
    let rawGreen = ColorPalette.calculateChannel(time,  bias.y, scale.y, oscillations.y, phase.y);
    let rawBlue  = ColorPalette.calculateChannel(time,  bias.z, scale.z, oscillations.z, phase.z);

    if (clampSaturation) {
      rawRed   = clamp01(rawRed);
      rawGreen = clamp01(rawGreen);
      rawBlue  = clamp01(rawBlue);
    }

    const point = {
      red: rawRed,
      green: rawGreen,
      blue: rawBlue,
    }

    points.push(point);
  }

  return (
    <ResponsiveContainer height={200}>
      <LineChart data={points}>
        <YAxis type={"number"} interval={'preserveStartEnd'}/>
        {/*<XAxis/>*/}
        <ReferenceLine y={0} label="" stroke="#BB000066"/>
        <ReferenceLine y={1} label="" stroke="#BB000066"/>
        <Line
          type={'monotone'}
          dataKey={'red'}
          stroke={'#BB0000CC'}
          strokeWidth={2}
          dot={null}
        />
        <Line
          type={'monotone'}
          dataKey={'green'}
          stroke={'#00BB00CC'}
          strokeWidth={2}
          dot={null}
        />
        <Line
          type={'monotone'}
          dataKey={'blue'}
          stroke={'#0000BBCC'}
          strokeWidth={2}
          dot={null}
        />
      </LineChart>
    </ResponsiveContainer>
  );
};
