// @flow
import React from 'react';
import InputPixelSize from "../Inputs/InputPixelSize";
import type {PngOptionsType} from "../Exporter/PngExporter";

type Props = {
  options: PngOptionsType,
  updateOptions: Function<Object>,
};

export default function PngOptions(props: Props) {
  const {options, updateOptions} = props;

  const updateOption = (key: string, value) => {
    options[key] = value;
    updateOptions(options);
  }

  return (
    <>
      <hr/>
      <InputPixelSize values={options.pixelSize} onChange={(newValues) => updateOption('pixelSize', newValues)}/>
    </>
  );
}
