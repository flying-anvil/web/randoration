// @flow
import React from 'react';
import {loadLocalStorageNamed} from "./Exporter/LocalStorageExporter";
import {Button} from "react-bootstrap";
import {FaExclamationTriangle} from "react-icons/all";

type Props = {
  startExport: Function,
  selectedFormat: string,
  exportName: string,
};

export default function ExportButton(props: Props) {
  const {startExport, selectedFormat, exportName} = props;
  const isLocalStorage = selectedFormat === 'localStorage';

  let text = 'Export';

  if (isLocalStorage) {
    const isSaved = loadLocalStorageNamed(exportName);

    text = isSaved
      ? 'Overwrite'
      : 'Save';
  }

  return (
    <>
      <Button
        variant={'success'}
        onClick={startExport}
        title={isLocalStorage ? 'There is no built-in way to load a stored palette yet.' : null}
      >
        {text}
        {isLocalStorage && <FaExclamationTriangle className={'ml-2'}/>}
      </Button>
    </>
  );
}
