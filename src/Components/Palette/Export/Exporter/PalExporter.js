// @flow

import type {ParametersState} from "../../parameterReducer";
import Color from "../../../../Classes/DataObject/Color";
import {downloadData} from "../../../../Helper/Download";

export function exportPal(name: string, parameters: ParametersState, colors: Array<Color>, exportDate: Date) {
  const length = colors.length * 3;

  const data = new Uint8Array(length);

  for (const index in colors) {
    const byteIndex = index * 3;
    // noinspection JSUnfilteredForInLoop
    const color = colors[index];

    data[byteIndex] = color.red;
    data[byteIndex + 1] = color.green;
    data[byteIndex + 2] = color.blue;
  }

  downloadData(data, `${name}.pal`, 'application/octet-stream');
}
