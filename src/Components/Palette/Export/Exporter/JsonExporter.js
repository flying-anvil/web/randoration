// @flow

import type {ParametersState} from "../../parameterReducer";
import Color from "../../../../Classes/DataObject/Color";
import {downloadData} from "../../../../Helper/Download";

export function exportJson(name: string, parameters: ParametersState, colors: Array<Color>, exportDate: Date) {
  const data = {
    exportDate: exportDate,
    parameters,
    colors,
  }

  downloadData(JSON.stringify(data, null, 2), `${name}.json`, 'application/json');
}
