// @flow

import type {ParametersState} from "../../parameterReducer";
import Color from "../../../../Classes/DataObject/Color";
import {downloadBlob} from "../../../../Helper/Download";

export type PngOptionsType = {
  pixelSize: {
    x: number,
    y : number,
  },
}

const defaultPngOptions: PngOptionsType = {
  pixelSize: {
    x: 16,
    y: 16,
  }
}

export {defaultPngOptions};

export function exportPng(name: string, parameters: ParametersState, colors: Array<Color>, exportDate: Date, specificOptions: PngOptionsType) {
  const {pixelSize} = specificOptions;

  const canvas = document.createElement('canvas');
  canvas.width = colors.length * pixelSize.x;
  canvas.height = pixelSize.y;

  const ctx = canvas.getContext('2d');

  for (const index in colors) {
    // noinspection JSUnfilteredForInLoop
    const color = colors[index];
    const x = index * pixelSize.x;

    ctx.fillStyle = color.toHex();
    ctx.fillRect(x,  0, pixelSize.x, pixelSize.y);
  }

  canvas.toBlob((blob) => {
    downloadBlob(blob, `${name}.png`, 'image/png');
  })
}
