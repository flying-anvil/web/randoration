// @flow

import type {ParametersState} from "../../parameterReducer";
import Color from "../../../../Classes/DataObject/Color";

const storageKey = 'generatedPalettes';

export function exportLocalStorage(name: string, parameters: ParametersState, colors: Array<Color>, exportDate: Date) {
  const currentSaved = loadLocalStorageAll();

  console.log(currentSaved);

  const data = {
    exportDate: exportDate,
    parameters,
  }

  const newSave = {
    ...currentSaved,
    [name]: data,
  };

  console.log(newSave);

  localStorage.setItem(storageKey,  JSON.stringify(newSave));
}

export function loadLocalStorageAll() {
  return JSON.parse(localStorage.getItem(storageKey) || '{}');
}

export function loadLocalStorageNamed(name: string) {
  const all = loadLocalStorageAll();

  return all[name] || null;
}

export {storageKey as paletteExportLocalStorageKey};
