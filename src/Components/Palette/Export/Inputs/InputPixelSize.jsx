// @flow
import React, {useState} from 'react';
import {Col, Form, FormGroup, Row} from "react-bootstrap";
import {defaultPngOptions} from "../Exporter/PngExporter";
import {clamp} from "../../../../Helper/Math/Clamp";

type Props = {
  values?: {
    x: number,
    y: number,
  },
  onChange: Function,
};

export default function InputPixelSize(props: Props) {
  const {
    values = {},
    onChange,
  } = props;

  const [rawValues, setRawValues] = useState(defaultPngOptions.pixelSize);

  const onRawChange = (event) => {
    const rawName = event.target.name;
    const name = rawName.split('.')[1];

    const newValues = {
      ...rawValues,
      [name]: clamp(parseInt(event.target.value), 1, 128),
    }

    setRawValues(newValues);

    onChange && onChange(newValues);
  }

  return (
    <FormGroup as={Row}>
      <Col md={2} className={'pl-0 pr-0'}>
        <Form.Label column='sm'>Pixel Size</Form.Label>
      </Col>
      <Col md={5}>
        <Form.Control
          type={'number'}
          name={'export-pixel-size.x'}
          value={values.x || undefined}
          min={1}
          max={128}
          onChange={onRawChange}
        />
      </Col>
      <Col md={5}>
        <Form.Control
          type={'number'}
          name={'export-pixel-size.y'}
          value={values.y || undefined}
          min={1}
          max={128}
          onChange={onRawChange}
        />
      </Col>
    </FormGroup>
  );
};
