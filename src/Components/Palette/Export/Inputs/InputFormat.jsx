// @flow
import React from 'react';
import {Col, Form, Row} from "react-bootstrap";

type Props = {
  onChange: Function,
  selected: string,
  formats: {
    [string]: string,
  },
}

export default function InputFormat(props: Props) {
  const {onChange, selected, formats} = props;

  return (
    <Form.Group as={Row} controlId={'export-format'}>
      <Col md={2} className={'pl-0 pr-0'}>
        <Form.Label column='sm'>Export Format</Form.Label>
      </Col>
      <Col className={'pl-0 pr-0'}>
        <Form.Control as="select" onChange={onChange} value={selected} custom>
          {Object.keys(formats).map((key: string, index: number) =>
            <React.Fragment key={key}>
              {index === 7 && <option disabled>{'╴ '.repeat(12)}</option>}
              <option
                value={key}
              >
                {formats[key]}
              </option>
            </React.Fragment>
          )}
        </Form.Control>
      </Col>
    </Form.Group>
  );
};
