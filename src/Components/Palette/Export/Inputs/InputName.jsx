// @flow
import React from 'react';
import {Col, Form, Row} from "react-bootstrap";

type Props = {
  value: string,
  onChange: Function,
};

export default function InputName(props: Props) {
  const {value, onChange} = props;

  return (
    <Form.Group as={Row} controlId={'export-name'}>
      <Col md={2} className={'pl-0 pr-0'}>
        <Form.Label column='sm'>Export Name</Form.Label>
      </Col>
      <Col className={'pl-0 pr-0'}>
        <Form.Control
          type={'text'}
          value={value}
          onChange={onChange}
        />
      </Col>
    </Form.Group>
  );
};
