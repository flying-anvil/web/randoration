// @flow
import React, {useState} from 'react';
import {Button, Col, Form, Modal} from "react-bootstrap";
import type {ParametersState} from "../parameterReducer";
import Color from "../../../Classes/DataObject/Color";
import InputName from "./Inputs/InputName";
import InputFormat from "./Inputs/InputFormat";
import {exportJson} from "./Exporter/JsonExporter";
import {exportPal} from "./Exporter/PalExporter";
import PngOptions from "./SpecificOptions/PngOptions";
import {defaultPngOptions, exportPng} from "./Exporter/PngExporter";
import {exportLocalStorage} from "./Exporter/LocalStorageExporter";
import ExportButton from "./ExportButton";

type Props = {
  isOpen: boolean,
  close: Function<>,
  parameters: ParametersState,
  colors: Array<Color>,
};

const exportFormats = {
  json: 'JSON',
  pal: 'RGB Palette',
  png: 'Image (PNG)',
  localStorage: 'Browser (Local Storage)',
}

const doExport = (name: string, specificOptions: Object, format: string, parameters: ParametersState, colors: Array<Color>) => {
  const exportTime = new Date();
  const formatOptions = specificOptions[format] || {};

  switch (format) {
    case 'json':
      exportJson(name, parameters, colors, exportTime);
      break;
    case 'pal':
      exportPal(name, parameters, colors, exportTime);
      break;
    case 'png':
      exportPng(name, parameters, colors, exportTime, formatOptions);
      break;
    case 'localStorage':
      exportLocalStorage(name, parameters, colors, exportTime);
      break;
    default: throw new Error(`Unknown export format "${format}"`);
  }
}

const renderFormatSpecificInputs = (format: string, options: Object, updateOptions: Function<string, Object>) => {
  const specificOptions = options[format] || {};

  switch (format) {
    case 'png':
      return <PngOptions options={specificOptions} updateOptions={(specificOptions) => updateOptions(format, specificOptions)}/>
    default: return null;
  }
}

export default function ExportModal(props: Props) {
  const {isOpen, close, parameters, colors} = props;

  const [name, setName]     = useState(() => `exported_palette`);
  const [format, setFormat] = useState('json');
  const [canExport, setCanExport] = useState(true);
  const [key, setKey] = useState(0);
  const [specificOptions, setSpecificOptions] = useState({
    png: defaultPngOptions,
  });

  const startExport = (event = null) => {
    if (!canExport && false) return;

    try {
      doExport(name, specificOptions, format, parameters, colors, event && event.preventDefault())
      setCanExport(false);
      setKey(key + 1);
    } catch (error) {
      console.error(error);
    }
  }

  const updateSpecificOptions = (format: string, options: Object) => {
    setSpecificOptions({...specificOptions, [format]: options});
  }

  return (
    <Modal show={isOpen} size={'lg'} className={'mt-5'} onHide={close}>
      <Modal.Header closeButton>
        <Modal.Title>Export</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Col>
          <Form onSubmit={startExport}>
            <InputName value={name} onChange={(event) => setName(event.target.value)}/>
            <InputFormat formats={exportFormats} selected={format} onChange={(event) => setFormat(event.target.value)}/>

            {renderFormatSpecificInputs(format, specificOptions, updateSpecificOptions)}

            <Button type={"submit"} hidden>&nbsp;</Button>
          </Form>
        </Col>
      </Modal.Body>
      <Modal.Footer>
        <ExportButton
          startExport={startExport}
          selectedFormat={format}
          exportName={name}
          key={key}
        />
      </Modal.Footer>
    </Modal>
  );
};
