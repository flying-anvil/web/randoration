// @flow
import React, {useState} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import type {ParametersState} from "./parameterReducer";
import ColorPalette from "../../Classes/Palette/ColorPalette";
import {CgSoftwareDownload} from "react-icons/all";
import ExportModal from "./Export/ExportModal";

type Props = {
  parameters: ParametersState,
  palette: ColorPalette,
};

export default function PaletteGeneratorHeader(props: Props) {
  const {parameters, palette} = props;

  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = () => {
    setIsOpen(!isOpen);
  }

  return (
    <Row>
      <Col>
        <h5>Palette Generator</h5>
      </Col>
      <Col className={'text-right'}>
        <Button
          variant={'secondary'}
          onClick={toggleOpen}
          size={'sm'}
        >
          Export <CgSoftwareDownload/>
        </Button>

        <ExportModal
          isOpen={isOpen}
          close={() => setIsOpen(false)}
          parameters={parameters}
          colors={palette.colors}
        />
      </Col>
    </Row>
  );
};
