// @flow

import {clamp} from "../../Helper/Math/Clamp";

export type OptionState = {
  colorCount: number,
  clampSaturation: boolean,
  showNumbers: boolean,
}

type Action = {
  option: string,
  value: number|boolean,
}

export default function optionReducer(state: OptionState, action: Action): OptionState {
  const {option, value} = action;

  let newValue = value;

  if (option === 'colorCount') {
    newValue = clamp(value, 0, 2 ** 10)
  }

  return {
    ...state,
    [option]: newValue,
  }
}
