// @flow
import React from 'react';
import {Col, Form, Row} from "react-bootstrap";
import type {OptionState} from "./optionReducer";

type Props = {
  options: OptionState,
  onOptionChange: Function<string, number|boolean>,
};

export default function OptionControls(props: Props) {
  const {options, onOptionChange} = props;
  const {colorCount, clampSaturation, showNumbers} = options;

  const onChange = (event) => {
    const newValue = event.target.type === 'checkbox'
      ? event.target.checked
      : event.target.value;

    onOptionChange(event.target.name, newValue);
  }

  return (
    <Row>
      <Col>
        <Form inline>
          <Form.Label htmlFor={'number-color-count'} className={'mr-2'}>Color Count</Form.Label>
          <Form.Control
            type="number"
            name={'colorCount'}
            id={'number-color-count'}
            value={colorCount}
            onChange={onChange}
          />
        </Form>
      </Col>

      <Col>
        <Form.Group controlId="check-clamp-saturation">
          <Form.Check
            type="checkbox"
            label="Clamp Saturation"
            name={'clampSaturation'}
            checked={clampSaturation}
            onChange={onChange}
          />
        </Form.Group>
      </Col>

      <Col>
        <Form.Group controlId="check-show-numbers">
          <Form.Check
            type="checkbox"
            label="Show Numbers"
            name={'showNumbers'}
            checked={showNumbers}
            onChange={onChange}
          />
        </Form.Group>
      </Col>
    </Row>
  );
};
