// @flow
import React, {useReducer} from 'react';
import {Card, Col, Row} from "react-bootstrap";
import ColorPalette from '../../Classes/Palette/ColorPalette';
import Vector3 from "../../Classes/Math/Vector3";
import ParameterControls from "./ParameterControls";
import parameterReducer from "./parameterReducer";
import type {ParametersState} from "./parameterReducer";
import OptionControls from "./OptionControls";
import type {OptionState} from "./optionReducer";
import optionReducer from "./optionReducer";
import PaletteDisplay from "./PaletteDisplay";
import DualRange from "../Inputs/DualRange";
import ParameterGraphDisplay from "./ParameterGraphDisplay";
import PalettePresets from "./AdditionalFeatures/PalettePresets";
import PaletteGeneratorHeader from "./PaletteGeneratorHeader";
import PaletteTabs from "./AdditionalFeatures/PaletteTabs";

type Props = {};

const defaultBias         = new Vector3(0.5, 0.5, 0.5);
const defaultScale        = new Vector3(0.5, 0.5, 0.5);
const defaultOscillations = new Vector3(1.0, 1.0, 1.0);
const defaultPhase        = new Vector3(0.0, .33, .67);

const defaultParameters: ParametersState = {
  bias: defaultBias,
  scale: defaultScale,
  oscillations: defaultOscillations,
  phase: defaultPhase,
};

const defaultOptions: OptionState = {
  colorCount: 24,
  clampSaturation: true,
  showNumbers: false,
};

export default function ColorPaletteGenerator(props: Props) {
  const [parameters, dispatchParameters] = useReducer<ParametersState>(parameterReducer, defaultParameters);
  const [options, dispatchOptions] = useReducer<OptionState>(optionReducer, defaultOptions);

  const {bias, scale, oscillations, phase} = parameters;
  const {colorCount, clampSaturation, showNumbers} = options;

  const palette = ColorPalette.generate(colorCount, bias, scale, oscillations, phase, clampSaturation);

  const onParameterChange = (parameter: string, channel: string, value: number) => {
    dispatchParameters({
      operation: 'partial',
      data: {parameter, channel, value},
    });
  }

  const onPresetLoad = (preset: ParametersState) => {
    dispatchParameters({
      operation: 'replace',
      data: preset,
    });
  }

  const onParameterChangeDelta = (deltaParameters: ParametersState) => {
    dispatchParameters({
      operation: 'delta',
      data: deltaParameters,
    });
  }

  const onOptionChange = (option: string, value: number|boolean) => {
    dispatchOptions({option, value});
  }

  return (
    <Row className={'ml-5 mr-5'}>
      <Col md={1}/>
      <Col md={10}>
        <Card>
          <Card.Header>
            <PaletteGeneratorHeader parameters={parameters} palette={palette}/>
          </Card.Header>
          <Card.Body>

            <OptionControls options={options} onOptionChange={onOptionChange}/>
            <hr/>

            <ParameterControls parameters={parameters} onParameterChange={onParameterChange} showNumbers={showNumbers}/>
            <hr/>

            <PaletteDisplay colors={palette.colors}/>
            <hr/>

            <ParameterGraphDisplay parameters={parameters} clampSaturation={clampSaturation} precision={50}/>
            <hr/>

            <PaletteTabs loadPreset={onPresetLoad} changeParametersDelta={onParameterChangeDelta}/>
            <hr/>

            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>

            <PalettePresets loadPreset={onPresetLoad}/>

            <hr/>
            <DualRange onChange={(values) => console.log(values)}/>

          </Card.Body>
        </Card>
      </Col>
      <Col md={1}/>
    </Row>
  );
};
