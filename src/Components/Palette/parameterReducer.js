// @flow

import Vector3 from "../../Classes/Math/Vector3";

export type ParametersState = {
  bias: Vector3,
  scale: Vector3,
  oscillations: Vector3,
  phase: Vector3,
}

type PartialData = {
  value: number,
  parameter: string,
  channel: string,
}

type Action = {
  operation: 'partial'|'replace'|'delta',
  data: PartialData|ParametersState,
}

function getNewVector(oldValue, channel, value) {
  switch (channel) {
    case 'red':   return new Vector3(     value, oldValue.y, oldValue.z);
    case 'green': return new Vector3(oldValue.x,      value, oldValue.z);
    case 'blue':  return new Vector3(oldValue.x, oldValue.y,      value);
    default: throw new Error(`Unknown color channel "${channel}"`);
  }
}

export default function parameterReducer(state: ParametersState, action: Action): ParametersState {
  const {operation, data} = action;

  switch (operation) {
    case 'partial':
      const {value, parameter, channel} = data;
      state[parameter] = getNewVector(state[parameter], channel, value);
      return {...state};
    case 'replace':
      return {...data};
    case 'delta':
      return {
        bias: Vector3.add(state.bias, data.bias),
        scale: Vector3.add(state.scale, data.scale),
        oscillations: Vector3.add(state.oscillations, data.oscillations),
        phase: Vector3.add(state.phase, data.phase),
      };
    default: throw new Error(`Unknown operation "${operation}"`);
  }
}
