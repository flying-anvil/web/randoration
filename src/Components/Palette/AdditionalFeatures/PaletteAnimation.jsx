// @flow
import React, {useCallback, useReducer, useState} from 'react';
import {Button, Col, Form, InputGroup, Row} from "react-bootstrap";
import ParameterControls from "../ParameterControls";
import Vector3 from "../../../Classes/Math/Vector3";
import type {ParametersState} from "../parameterReducer";
import parameterReducer from "../parameterReducer";
import {FaPlay, FaStop} from "react-icons/all";
import {useAnimationFrame} from "../../../Hooks/useAnimationFrame";
import type {Delta} from "../../../Hooks/useAnimationFrame";

const defaultBias         = new Vector3(0.00, 0.00, 0.00);
const defaultScale        = new Vector3(0.00, 0.00, 0.00);
const defaultOscillations = new Vector3(0.00, 0.00, 0.00);
const defaultPhase        = new Vector3(0.00, 0.00, 0.00);

const defaultParameters: ParametersState = {
  bias: defaultBias,
  scale: defaultScale,
  oscillations: defaultOscillations,
  phase: defaultPhase,
};

type Props = {
  changeParametersDelta: Function<ParametersState>,
};

export default function PaletteAnimation(props: Props) {
  const {changeParametersDelta} = props;

  const [animationParameters, dispatchAnimationParameters] = useReducer<ParametersState>(parameterReducer, defaultParameters);
  const [animationSpeed, setAnimationSpeed] = useState(0.1);
  const [isRunning, setRunning] = useState(false);

  const onParameterChange = (parameter: string, channel: string, value: number) => {
    dispatchAnimationParameters({
      operation: 'partial',
      data: {parameter, channel, value},
    });
  }

  const animation = useCallback((delta: Delta) => {
    const {bias, scale, oscillations, phase} = animationParameters;
    const multiplier = delta.s * animationSpeed;

    changeParametersDelta({
      bias: bias.multiply(multiplier),
      scale: scale.multiply(multiplier),
      oscillations: oscillations.multiply(multiplier),
      phase: phase.multiply(multiplier),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [animationParameters, animationSpeed]);

  useAnimationFrame(isRunning, animation)

  const updateAnimationSpeed = (event) => {
    const newValue = event.target.name === 'animation-animation-speed-number'
      ? parseFloat(event.target.value) * 0.01
      : parseFloat(event.target.value);

    setAnimationSpeed(newValue);
  }

  return (
    <>
      <ParameterControls parameters={animationParameters} onParameterChange={onParameterChange} showNumbers={true}/>
      <hr/>
      <Row>
        <Col md={1}>
          <Button
            variant={isRunning ? 'danger' : 'success'}
            onClick={() => setRunning(!isRunning)}
          >{isRunning ? 'Stop' : 'Start'} <span className={'ml-1'}>{isRunning ? <FaStop/> : <FaPlay/>}</span>
          </Button>
        </Col>
        <Col>
          <Form.Group as={Row} controlId={'animation-speed'}>
            <Col md={2}>
              <Form.Label>Animation  Speed</Form.Label>
            </Col>
            <Col>
              <Form.Control
                type={'range'}
                name={'animation-animation-speed-range'}
                min={0}
                max={1}
                step={0.00001}
                onChange={updateAnimationSpeed}
                value={animationSpeed}
                custom
              />
            </Col>
            <Col md={2}>
              <InputGroup>
                <Form.Control
                  type={'number'}
                  name={'animation-animation-speed-number'}
                  min={0}
                  max={100}
                  step={0.1}
                  value={(animationSpeed * 100).toFixed(1)}
                  onChange={updateAnimationSpeed}
                />
                <InputGroup.Append><InputGroup.Text>%</InputGroup.Text></InputGroup.Append>
              </InputGroup>
            </Col>
          </Form.Group>
        </Col>
      </Row>
    </>
  );
}
