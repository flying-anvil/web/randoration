// @flow
import * as React from 'react';

type Props = {
  children: React.Node,
  name: string,
  activeTab: string,
  setActive: Function,
};

const defaultClasses = 'cursor-pointer nav-item nav-link';

export default function TabButton(props: Props) {
  const {children, name, activeTab, setActive} = props;
  const isActive = name === activeTab;

  return (
      <span
        className={`${defaultClasses} ${isActive ? 'active' : ''}`}
        onClick={() => setActive(name)}
      >
        {children}
      </span>
  );
}
