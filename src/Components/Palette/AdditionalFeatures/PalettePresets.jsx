// @flow
import React, {useState} from 'react';
import {Button, Col, Form, Row} from "react-bootstrap";
import presets from "../../../Data/Palette/presets";
import type {PresetType} from "../../../Data/Palette/presets";
import type {ParametersState} from "../parameterReducer";

type Props = {
  loadPreset: Function<ParametersState>,
};

export default function PalettePresets(props: Props) {
  const {loadPreset} = props;

  const [selectedIndex, setSelectedIndex] = useState(0);

  const onChange = (event) => {
    const index = event.target.value;

    setSelectedIndex(index);
    reloadPreset(index)
  }

  const reloadPreset = (index?: number) => {
    const preset = presets[index || selectedIndex];
    if (!preset) return;

    loadPreset({
      bias: preset.bias,
      scale: preset.scale,
      oscillations: preset.oscillations,
      phase: preset.phase,
    });
  }

  return (
    <Row>
      <Col md={4}>
        <Form.Group controlId="select-preset">
          {/*<Form.Label>Load Preset</Form.Label>*/}
          <Form.Control as="select" onChange={onChange}>
            {presets.map((preset: PresetType, index: number) =>
              <React.Fragment key={preset.name}>
                {index === 7 && <option disabled>{'╴ '.repeat(12)}</option>}
                <option
                  value={index}
                >
                  {`${index} – ${preset.name}`}
                </option>
              </React.Fragment>
            )}
          </Form.Control>
        </Form.Group>
      </Col>
      <Col md={1}>
        <Button onClick={reloadPreset}>Re-Load</Button>
      </Col>
    </Row>
  );
};
