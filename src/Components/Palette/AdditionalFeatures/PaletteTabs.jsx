// @flow
import React, {useState} from 'react';
import {Card, Col, Row} from "react-bootstrap";
import PalettePresets from "./PalettePresets";
import type {ParametersState} from "../parameterReducer";
import TabButton from "./TabButton";
import PaletteAnimation from "./PaletteAnimation";
import {FaExclamationTriangle} from "react-icons/all";

type Props = {
  loadPreset: Function<ParametersState>,
  changeParametersDelta: Function<ParametersState>,
};

export default function PaletteTabs(props: Props) {
  const {loadPreset, changeParametersDelta} = props;

  const [activeTab, setActiveTab] = useState('presets');

  const renderActiveTab = () => {
    switch (activeTab) {
      case 'presets':
        return <PalettePresets loadPreset={loadPreset}/>;
      case 'animation':
        return <PaletteAnimation changeParametersDelta={changeParametersDelta}/>;
      default:
        return null;
    }
  }

  return (
    <>
      <Card>
        <Card.Header>
          <Row>
            <nav className="cursor-pointer nav nav-tabs" role="tablist">
              <TabButton name={'presets'} activeTab={activeTab} setActive={setActiveTab}>Presets</TabButton>
              <TabButton name={'animation'} activeTab={activeTab} setActive={setActiveTab}>Animation</TabButton>
              <TabButton name={'random'} activeTab={activeTab} setActive={setActiveTab}>Random</TabButton>
            </nav>
            <Col className={'text-right'}>
              {['animation', 'random'].includes(activeTab) &&
              <span className={'btn btn-outline-warning cursor-default'}>
                <FaExclamationTriangle/>&nbsp;Experimental
              </span>
              }
            </Col>
          </Row>
        </Card.Header>
        <Card.Body>
          {renderActiveTab()}
        {/*  <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">*/}
        {/*    <Tab eventKey="presets" title="Presets" tabClassName={'mb-4'}>*/}
        {/*      <PalettePresets loadPreset={loadPreset}/>*/}
        {/*    </Tab>*/}
        {/*    <Tab eventKey="animation" title="Animation">*/}
        {/*      Lorem Ipsum dolor sit amet…2*/}
        {/*    </Tab>*/}
        {/*    <Tab eventKey="random" title="Random">*/}
        {/*      Lorem Ipsum dolor sit amet…3*/}
        {/*    </Tab>*/}
        {/*  </Tabs>*/}
        </Card.Body>
      </Card>
    </>
  );
}
