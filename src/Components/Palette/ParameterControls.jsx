// @flow
import React from 'react';
import {Col, Form, Row} from "react-bootstrap";
import {upperCaseFirst} from "../../Helper/StringHelper";
import allParameters, {parameterNames} from "../../Helper/PaletteGenerator/ParameterNames";
import type {ParametersState} from "./parameterReducer";

type Props = {
  parameters: ParametersState,
  onParameterChange: Function<string, string, number>,
  showNumbers?: boolean,
};

const colorChannels = ['red', 'green', 'blue'];
const mappingColorChannelToXyz = {
  'red':   'x',
  'green': 'y',
  'blue':  'z',
}

const mappingParameterMin = {
  [parameterNames.bias]: 0,
  [parameterNames.scale]: -1,
  [parameterNames.oscillations]: 0,
  [parameterNames.phase]: 0,
}

const mappingParameterMax = {
  [parameterNames.bias]: 1,
  [parameterNames.scale]: 1,
  [parameterNames.oscillations]: 4,
  [parameterNames.phase]: 1,
}

export default function ParameterControls(props: Props) {
  const {parameters, onParameterChange, showNumbers = false} = props;

  const onChange = (event) => {
    const name = event.target.name;

    const [parameter, channel] = name.split('.');
    const value = parseFloat(event.target.value);

    onParameterChange(parameter, channel, value);
  }

  return (
    <>
      <Row className={'mb-2'}>
        <Col md={1}>&nbsp;</Col>
        {allParameters.map((parameter: string) => <Col key={parameter}>{upperCaseFirst(parameter)}</Col>)}
      </Row>
      {colorChannels.map((channel: string) => {
        return <Row className={'mb-2'} key={channel}>
          <Col md={1}>
            {upperCaseFirst(channel)}
          </Col>
          {allParameters.map((parameter: string) =>
            <Col key={parameter} style={{borderLeft: '2px solid #DEE2E6'}}>
              <Row>
                <Col>
                  <Form.Control
                    type={'range'}
                    name={`${parameter}.${channel}.range`}
                    min={mappingParameterMin[parameter]}
                    max={mappingParameterMax[parameter]}
                    step={0.01}
                    value={parameters[parameter][mappingColorChannelToXyz[channel]]}
                    onChange={onChange}
                    custom
                  />
                </Col>
                {showNumbers &&
                <Col md={4}>
                  <Form.Control
                    type={'number'}
                    name={`${parameter}.${channel}.number`}
                    min={mappingParameterMin[parameter]}
                    max={mappingParameterMax[parameter]}
                    step={0.01}
                    value={parameters[parameter][mappingColorChannelToXyz[channel]]}
                    onChange={onChange}
                  />
                </Col>
                }
              </Row>
            </Col>
          )}
        </Row>
      })}
    </>
  );
};
