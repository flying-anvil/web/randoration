// @flow
import React from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import NavItem from "./NavItem";
import DebugDropdown from "./Debug/DebugDropdown";
import SetDropdown from "./Set/SetDropdown";

type Props = {};

export default function Navigation(props: Props) {
  return (
    <>
      <Navbar>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Link to={'/'} className="navbar-brand">Randoration</Link>

          <Nav className="mr-auto">
            <SetDropdown/>
            {/*<NavItem as={Link} to={'/environment'} text={'Environment'}/>*/}
            <NavItem as={Link} to={'/palette'} text={'Color-Palette'}/>
            <NavItem as={Link} to={'/management/saved'} text={'Saved'}/>
            <DebugDropdown/>
          </Nav>

          {/*<Nav className="mr-auto">*/}
          {/*    <NavItem as={Link} to={'/environment'} text={'Environment'}/>*/}

          {/*  <NavDropdown title="Debug" id="basic-nav-dropdown-debug">*/}
          {/*    <NavDropdown title="Sets" id="basic-nav-dropdown-debug-sets">*/}
          {/*      <Nav.Item eventkey={1} href="/debug/sets/biome">*/}
          {/*        <Nav.Link as={Link} to={'/debug/sets/biome'}>All Biomes</Nav.Link>*/}
          {/*      </Nav.Item>*/}
          {/*    </NavDropdown>*/}
          {/*  </NavDropdown>*/}
          {/*</Nav>*/}
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};
