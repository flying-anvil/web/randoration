// @flow
import React from 'react';
import {Nav, NavDropdown} from "react-bootstrap";
import {Link} from "react-router-dom";

type Props = {};

type SetsType = {
  name: string,
  navigationTarget: string,
}

export default function SetDropdown(props: Props) {
  const sets: Array<SetsType> = [
    {
      name: 'Environment',
      navigationTarget: 'environment',
    },
    {
      name: 'Tileset',
      navigationTarget: 'tileset',
    },
  ];

  return (
    <NavDropdown title="Sets" id="basic-nav-dropdown-debug">
      {Object.values(sets).map((set: SetsType) => {
        return <NavDropdown.Item eventkey={1} as={'div'} key={set.name}>
          <Nav.Link as={Link} to={`/set/${set.navigationTarget}`}>{set.name}</Nav.Link>
        </NavDropdown.Item>
      })}
    </NavDropdown>
  );
};
