// @flow
import React from 'react';
import {Nav, NavDropdown} from "react-bootstrap";
import {Link} from "react-router-dom";
import {AllSets} from "../../../Data/Sets/AllSets";

type Props = {};

export default function DebugDropdown(props: Props) {
  return (
    <NavDropdown title="Debug" id="basic-nav-dropdown-debug">
      <NavDropdown title="Sets" id="basic-nav-dropdown-debug-sets">
        {Object.values(AllSets).map((set) => {
          const name = set.meta.name;

          return <NavDropdown.Item eventkey={1} as={'div'} key={name}>
            <Nav.Link as={Link} to={`/debug/sets/${name.toLowerCase()}`}>All {name}s</Nav.Link>
          </NavDropdown.Item>
        })}
      </NavDropdown>
    </NavDropdown>
  );
};
