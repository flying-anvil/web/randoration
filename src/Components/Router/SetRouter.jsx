// @flow
import React from 'react';
import {Route, Switch, useRouteMatch} from "react-router-dom";
import Environment from "../RandomSet/Environment";
import Tileset from "../RandomSet/Tileset";

type Props = {};

export default function SetRouter(props: Props) {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route exact path={`${path}/environment`}>
        <Environment/>
      </Route>
      <Route exact path={`${path}/tileset`}>
        <Tileset/>
      </Route>
    </Switch>
  );
}
