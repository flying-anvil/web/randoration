// @flow
import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Environment from "../RandomSet/Environment";
import Navigation from "../Navigation/Navigation";
import {Col} from "react-bootstrap";
import DebugSet from "../Elements/Debug/DebugSet";
import Saved from "../Managemend/Save/Saved";
import ColorPaletteGenerator from "../Palette/ColorPaletteGenerator";
import SetRouter from "./SetRouter";

type Props = {};

export default function Router(props: Props) {
  return (
    <BrowserRouter>
      <Navigation/>

      <Col md={12}>
        <Switch>

          <Route exact path={'/'}>
            <Environment/>
          </Route>

          <Route exact path={'/environment'}>
            <Environment/>
          </Route>

          <Route path={'/set'}>
            <SetRouter/>
          </Route>

          <Route exact path={'/palette'}>
            <ColorPaletteGenerator/>
          </Route>

          <Route exact path={'/management/saved'}>
            <Saved/>
          </Route>

          <Route exact path={'/debug/sets/:set'}>
            <DebugSet/>
          </Route>

        </Switch>
      </Col>
    </BrowserRouter>
  );
};
