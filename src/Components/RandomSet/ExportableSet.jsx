// @flow
import {useState} from 'react';
import type {SetMetaType, SetValueType} from "../../Data/Sets/Types/SetType";
import type {ExportType} from "../../Types/ExportType";
import {downloadData} from "../../Helper/Download";

type Props = {
  name: string,
  render: Function<Function, Function<SetValueType, ?SetMetaType>>
};

export default function ExportableSet(props: Props) {
  const {name, render} = props;

  const [exportableData: ExportType, setExportableData] = useState({meta: {name}, values: {}});

  const onChange = (newValue: SetValueType, set: ?SetMetaType, nameOverride: ?string) => {
    const newData = {...exportableData};
    const elementName = nameOverride || set.name;
    newData.values[elementName] = {
      name: elementName,
      value: newValue.name,
      color: newValue.color,
    }

    setExportableData(newData);
  }

  const onExport = () => {
    const json = JSON.stringify(exportableData, null, 2);
    downloadData(json, exportableData.meta.name);
  }

  return render(onChange, onExport);
};
