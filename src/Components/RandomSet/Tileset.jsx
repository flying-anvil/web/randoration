// @flow
import React from 'react';
import {Col, Row} from "react-bootstrap";
import GenericCard from "../Elements/Cards/GenericCard";
import Biomes from "../../Data/Sets/Biomes";
import Themes from "../../Data/Sets/Themes";
import Elements from "../../Data/Sets/Elements";
import Places from "../../Data/Sets/Places";
import BaseSet from "./BaseSet";
import TilesetType from "../../Data/Sets/Tileset/TilesetType";
import HtmlColors from "../../Data/Sets/HtmlColors";
import ColorCard from "../Elements/Cards/ColorCard";
import Materials from "../../Data/Sets/Materials";

type Props = {};

export default function Tileset(props: Props) {
  const smallMargin = 'mb-1';

  return (
    <BaseSet name={'Tileset'} render={(onChange) => (
      <>
        <Row className={'mb-4'}>
          <Col md={4} className={smallMargin}><GenericCard set={TilesetType} onChange={onChange}/></Col>
          <Col md={4} className={smallMargin}><ColorCard set={HtmlColors} nameOverride={'Primary Color'} onChange={onChange}/></Col>
          <Col md={4} className={smallMargin}><ColorCard set={HtmlColors} nameOverride={'Secondary Color'} onChange={onChange}/></Col>
        </Row>
        <hr/>
        <Row className={'mb-4'}>
          <Col md={5} className={smallMargin}><GenericCard set={Biomes} onChange={onChange}/></Col>
          <Col md={4} className={smallMargin}><GenericCard set={Elements} onChange={onChange}/></Col>
          <Col md={3} className={smallMargin}><GenericCard set={Materials} onChange={onChange}/></Col>
        </Row>
        <Row className={'mt-4'}>
          <Col md={12} className={smallMargin}><GenericCard set={Places} onChange={onChange}/></Col>
        </Row>
        <hr/>
      </>
    )}/>
  );
};
