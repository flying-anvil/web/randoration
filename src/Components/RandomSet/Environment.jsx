// @flow
import React, {useState} from 'react';
import {Card, Col, Row} from "react-bootstrap";
import GenericCard from "../Elements/Cards/GenericCard";
import Biomes from "../../Data/Sets/Biomes";
import Themes from "../../Data/Sets/Themes";
import Elements from "../../Data/Sets/Elements";
import RandomSetHeader from "./RandomSetHeader";
import ExportableSet from "./ExportableSet";
import Weathers from "../../Data/Sets/Weather";
import Places from "../../Data/Sets/Places";

type Props = {};

export default function Environment(props: Props) {
  const [seed, setSeed] = useState(0);

  const smallMargin = 'mb-1';

  return (
    <ExportableSet name={'Environment'} render={(onChange, onExport) => (
      <Row className={'ml-5 mr-5'}>
        <Col/>
        <Col md={9}>
          <Card>
            <Card.Header>
              <RandomSetHeader title={'Environment'} onRegenerate={() => setSeed(seed + 1)} onExport={onExport}/>
            </Card.Header>
            <Card.Body key={seed}>
              <Row className={'mb-4'}>
                <Col md={3} className={smallMargin}><GenericCard set={Biomes} onChange={onChange}/></Col>
                <Col md={5} className={smallMargin}><GenericCard set={Themes} onChange={onChange}/></Col>
                <Col md={4} className={smallMargin}><GenericCard set={Elements} onChange={onChange}/></Col>
              </Row>
              <Row className={'mt-4'}>
                <Col md={12} className={smallMargin}><GenericCard set={Places} onChange={onChange}/></Col>
              </Row>
              <hr/>
              <Row className={'mb-4'}>
                <Col md={7} className={smallMargin}><GenericCard set={Weathers} onChange={onChange} nameOverride={'Weather Primary'}/></Col>
                <Col md={5} className={smallMargin}><GenericCard set={Weathers} onChange={onChange} nameOverride={'Weather Secondary'}/></Col>
              </Row>
            </Card.Body>
          </Card>
        </Col>
        <Col />
      </Row>
    )}/>
  );
};
