// @flow
import React from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {BiRefresh, VscJson} from "react-icons/all";

type Props = {
  title: string,
  onExport?: Function,
  onRegenerate?: Function,
};

export default function RandomSetHeader(props: Props) {
  const {title, onExport, onRegenerate} = props;

  return (
    <Row>
      <Col>
        <h5>{title}</h5>
      </Col>
      <Col className={'text-right'}>
        {onExport &&
          <Button size={'sm'} variant={'secondary'} className={'ml-2'} onClick={onExport}>Export <VscJson/></Button>
        }

        {onRegenerate &&
          <Button size={'sm'} variant={'secondary'} className={'ml-2'} onClick={onRegenerate}>Regenerate <BiRefresh/></Button>
        }
      </Col>
    </Row>
  );
};
