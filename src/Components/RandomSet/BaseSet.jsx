// @flow
import React, {useState} from 'react';
import ExportableSet from "./ExportableSet";
import {Card, Col, Row} from "react-bootstrap";
import RandomSetHeader from "./RandomSetHeader";

type Props = {
  name: string,
  render: Function<Function>,
};

export default function BaseSet(props: Props) {
  const {name, render} = props;
  const [seed, setSeed] = useState(0);

  return (
    <ExportableSet name={name} render={(onChange, onExport) => (
      <Row className={'ml-5 mr-5'}>
        <Col/>
        <Col md={9}>
          <Card>
            <Card.Header>
              <RandomSetHeader title={name} onRegenerate={() => setSeed(seed + 1)} onExport={onExport}/>
            </Card.Header>
            <Card.Body key={seed}>
              {render()}
            </Card.Body>
          </Card>
        </Col>
        <Col />
      </Row>
    )}/>
  );
};
