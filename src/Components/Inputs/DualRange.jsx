// @flow
import React, {useState} from 'react';

type Props = {
  name?: string,
  min?: number,
  max?: number,
  step?: number,
  defaultValues?: Array<number>,
  values?: Array<number>,
  onChange?: Function<number, number>,
};

export default function DualRange(props: Props) {
  const {
    name = '',
    min = 0,
    max = 100,
    step = 1,
    defaultValues = [],
    values = [],
    onChange,
  } = props;

  const nameA = `${name}_a`;
  const nameB = `${name}_b`;

  const [rawValues, setRawValues] = useState({
    [nameA]: defaultValues[0] || (max / 3),
    [nameB]: defaultValues[1] || (2 * max / 3),
  });

  const onRawChange = (event) => {
    const newValues = {
      ...rawValues,
      [event.target.name]: parseFloat(event.target.value),
    }

    setRawValues(newValues);

    const sorted = Object.values(newValues).sort((a, b) => a - b);
    onChange && onChange(sorted);
  }

  return (
    <section className="dual-range">
      <span className="rangeValues"/>
      <input
        name={nameA}
        defaultValue={rawValues[nameA]}
        value={values[0] || undefined}
        min={min}
        max={max}
        step={step}
        type="range"
        onChange={onRawChange}
      />
      <input
        name={nameB}
        defaultValue={rawValues[nameB]}
        value={values[1] || undefined}
        min={min}
        max={max}
        step={step}
        type="range"
        onChange={onRawChange}
      />
    </section>
  );
};
