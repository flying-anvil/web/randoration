// @flow

const bias = 'bias';
const scale = 'scale';
const oscillations = 'oscillations';
const phase = 'phase';

const allParameters = [bias, scale, oscillations, phase];
const keyed = {bias, scale, oscillations, phase};

export default allParameters;
export {keyed as parameterNames}
