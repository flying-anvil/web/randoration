// @flow

import type {Color, ColorHSV} from "../Types/Color";

/**
 * @returns {Color} 0-255
 */
export const color1toColor255 = (color: Color): Color => {
  return {
    red: Math.round(color.red * 255),
    green: Math.round(color.green * 255),
    blue: Math.round(color.blue * 255),
  }
}

/**
 * @returns {Color} 0-1
 */
export const color255toColor1 = (color: Color): Color => {
  return {
    red: color.red / 255,
    green: color.green / 255,
    blue: color.blue / 255,
  }
}

/**
 * @see https://stackoverflow.com/a/17243070e
 *
 *
 * @param h Hue (Color)            0 to 1
 * @param s Saturation (Intensity) 0 to 1
 * @param v Value (Brightness)     0 to 1
 */
export const HSVtoRGB = (h, s, v): Color => {
  h = h % 1;
  s = s % 1;
  v = v % 1;

  let r, g, b, i, f, p, q, t;

  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0: r = v; g = t; b = p; break;
    case 1: r = q; g = v; b = p; break;
    case 2: r = p; g = v; b = t; break;
    case 3: r = p; g = q; b = v; break;
    case 4: r = t; g = p; b = v; break;
    case 5: r = v; g = p; b = q; break;
    default:
  }

  return {
    red: r,
    green: g,
    blue: b,
  };
}

/**
 * @see https://stackoverflow.com/a/54070620
 *
 * @param r 0-255
 * @param g 0-255
 * @param b 0-255
 *
 * @return {ColorHSV} (0-360, 0-1, 0-1)
 */
export const RGBtoHSV = (r, g, b): ColorHSV => {
  let v = Math.max(r, g, b), c = v - Math.min(r, g, b);
  let h = c && ((v === r) ? (g - b) / c : ((v === g) ? 2 + (b - r) / c : 4 + (r - g) / c));
  return {
      hue: 60 * (h < 0 ? h + 6 : h),
      saturation: v && c / v,
      value: v / 255,
    };
}

export const colorToHex = (color: Color) => {
  const paddedRed   = color.red.toString(16).padStart(2, '0');
  const paddedGreen = color.green.toString(16).padStart(2, '0');
  const paddedBlue  = color.blue.toString(16).padStart(2, '0');

  return `#${paddedRed}${paddedGreen}${paddedBlue}`;
}

/**
 * @returns {Color} 0-255
 */
export const hexToColor = (hex: string): Color => {
  if (hex[0] === '#') {
    hex = hex.substr(1);
  }

  return {
    red: parseInt(hex.substr(0, 2), 16),
    green: parseInt(hex.substr(2, 2), 16),
    blue: parseInt(hex.substr(4, 2), 16),
  };
}

/**
 * @see https://stackoverflow.com/a/596243
 * RGB in 0-1 range
 */
export const RGBtoLuminanceStandard = (r, g, b): number => (
  0.2126 * b + 0.7152 * g + 0.0722 * b
)

/**
 * @see https://stackoverflow.com/a/596243
 * RGB in 0-1 range
 */
export const RGBtoLuminancePerceived = (r, g, b): number => (
  0.299 * b + 0.587 * g + 0.114 * b
)

/**
 * @see https://stackoverflow.com/a/596243
 * RGB in 0-1 range
 */
export const RGBtoLuminancePerceived2 = (r, g, b): number => (
  0.299 * (b ** 2) + 0.587 * (g ** 2) + 0.114 * (b ** 2)
)

export const invertHexColor = (hexColor: string) => {
  const asNumber = parseInt(hexColor.substr(1), 16);
  const inverted = 0xFF_FF_FF - asNumber;
  return `#${inverted.toString(16).padStart(6, '0')}`;
}

export const brightenDarkHexColor = (hexColor: string, nullFallback: boolean = true) => {
  const {red, green, blue} = color255toColor1(hexToColor(hexColor));
  const luminance = RGBtoLuminancePerceived(red, green, blue);

  if (luminance <= .2) {
    return '#C9C0BB';
  }

  if (luminance <= .4) {
    return '#F0EAD6';
  }

  return nullFallback ? null : hexColor;
}

window.invertHexColor = invertHexColor;
window.brightenDarkHexColor = brightenDarkHexColor;
window.hexToHSV = (input) => {
  const {red, green, blue} = hexToColor(input);
  return RGBtoHSV(red, green, blue);
};
window.hexToLuminanceStandard = (input) => {
  const {red, green, blue} = color255toColor1(hexToColor(input));
  return RGBtoLuminanceStandard(red, green, blue);
};
window.hexToLuminancePerceived = (input) => {
  const {red, green, blue} = color255toColor1(hexToColor(input));
  return RGBtoLuminancePerceived(red, green, blue);
};
window.hexToLuminancePerceived2 = (input) => {
  const {red, green, blue} = color255toColor1(hexToColor(input));
  return RGBtoLuminancePerceived2(red, green, blue);
};
