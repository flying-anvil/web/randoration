// @flow

export const filterAlphanumerics = (input: string): string => {
  return input.replace(/[^a-zA-Z0-9]*/, '');
}

export const upperCaseFirst = (input: string): string => {
  return input.charAt(0).toUpperCase() + input.slice(1)
}

/**
 * @see https://stackoverflow.com/a/7616484
 */
export const stringHash = (input: string): number => {
  let hash = 0;

  if (input.length === 0) return hash;

  for (let i = 0; i < input.length; i++) {
    const chr = input.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }

  return hash;
};

export const stringHashAbs = (input: string): number => {
  return Math.abs(stringHash(input));
}
