// @flow

export function arrayChunks<T>(array: Array<T>, chunkSize: number): Array<Array<T>> {
  // const chunkCount = Math.ceil(array.length / chunkSize);

  const chunks = [];

  for (let i = 0; i < array.length; i += chunkSize) {
    const chunk = array.slice(i, i + 3)
    chunks.push(chunk);
  }

  return chunks;
}
