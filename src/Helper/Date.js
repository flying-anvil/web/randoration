// @flow

export function formatDateDate(date: Date) {
  const month = date.getMonth().toString().padStart(2, '0');
  const day   = date.getDay().toString().padStart(2, '0');

  return `${date.getFullYear()}-${month}-${day}`;
}
