// @flow

import type {SetType, SetValueType} from "../Data/Sets/Types/SetType";
import {stringHashAbs} from "./StringHelper";
import {color1toColor255, colorToHex, HSVtoRGB} from "./ColorConversion";
import {SetWeight} from "../Data/Sets/Constants";

export function fillWithDefaultValues(set: SetType) {
  return {
    meta: set.meta,
    values: set.values.map((current) => fillSingleWithDefaultValues(current)),
  }
}

export function fillSingleWithDefaultValues(value: SetValueType) {
  const formatted = {...value};

  if (!formatted.weight) {
    formatted.weight = SetWeight.normal;
  }

  if (!formatted.color) {
    const hue = (stringHashAbs(formatted.name) % 360) / 360;
    formatted.color = color1toColor255(HSVtoRGB(hue, .8, .9));
  }

  // TODO: Keep RGB/HSV
  if (typeof formatted.color !== 'string') {
    formatted.color = colorToHex(formatted.color);
  }

  return formatted;
}
