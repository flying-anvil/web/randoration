// @flow

import type {SetType, SetValueType} from "../Data/Sets/Types/SetType";
import {fillSingleWithDefaultValues} from "./SetFormatter";

const getTotalWeight = (values: Array<SetValueType>) => {
  let total = 0;
  for (const value of values) {
    total += value.weight || 1;
  }

  return total;
}

const findPicked = (set: SetType, picked: number): SetValueType => {
  let current = picked;

  for (const value of set.values) {
    current -= value.weight || 1;

    if (current <= 0) {
      return value;
    }
  }
}

export const pickRandomFromSet = (set: SetType): SetValueType => {
  const totalWeight = getTotalWeight(set.values);
  const weightedPicked = Math.ceil(Math.random() * totalWeight);

  const picked = {...findPicked(set, weightedPicked)};

  return fillSingleWithDefaultValues(picked);
}

export type DebugFairnesType = {
  [string]: number,
}

export const debugFairnes = (set: SetType): DebugFairnesType => {
  const totalWeight = getTotalWeight(set.values);
  const stats = {};

  for (let i = 0; i < 1000000; i++) {
    const picked = Math.ceil(Math.random() * totalWeight);

    if (stats[picked] === undefined) {
      stats[picked] = 0;
    }

    stats[picked]++;
  }

  return stats;
}

export const debugWieghtPicking = (set: SetType, count: number = 1000) => {
  const stats = {};

  for (let i = 0; i < count; i++) {
    const picked = pickRandomFromSet(set);

    if (stats[picked.name] === undefined) {
      stats[picked.name] = 0;
    }

    stats[picked.name]++;
  }

  return stats;
}
