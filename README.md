# Randoration

**Rando**m Inspi**ration**

Generates a set of random things you can use as a base for creative stuff.

Intended for environments.

## ToDo

Lock mechanism so that not all cards are rerolled.

## Development

### New Sets

Located under `/src/Sets/`, orientate structure from existing.
Register the new set at `AllSets` to make it appear in the debug pages.
